#include "base_serial.h"

#include "tools_no_ros.h"

#include <sys/time.h>
#include <fstream>


BaseSerial::~BaseSerial() {
    LOG(INFO) << "Goodbye BaseSerial..";
}

void BaseSerial::SetSerialDevice(const std::string &serialDevice) {
    LOG(INFO) << __FUNCTION__ << " start.";
    serialDevice_ = serialDevice;
}

void BaseSerial::SetBaudRate(const std::string &baudRate) {
    LOG(INFO) << __FUNCTION__ << " start.";
    baudRate_ = public_tools::ToolsNoRos::string2int(baudRate);
}

double BaseSerial::FilterDeque(const std::deque<double> &aDeque) {
    DLOG(INFO) << __FUNCTION__ << " start, aDeque.size(): " << aDeque.size();
    return (*std::min_element(aDeque.cbegin(), aDeque.cend()));
}

void BaseSerial::WriteTimeErrFile(double unixTime, double timeErr) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    const std::string timeErrFileName(outputFileName_ + "_time_err.txt");
    std::fstream file(timeErrFileName, std::ios::out | std::ios::app);
    if (!file) {
        LOG(ERROR) << "Failed to open " << timeErrFileName;
        exit(1);
    }

    file << std::fixed << unixTime << "," << timeErr << "\n";
    file.close();
}

