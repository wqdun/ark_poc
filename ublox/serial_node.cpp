#include <glog/logging.h>

#include "serial_factory.h"
#include "base_serial.h"


int main(int argc, char *argv[]) {
    google::InitGoogleLogging(argv[0]);
    LOG(INFO) << "Got " << argc << " parameters.";
    if (argc < 4) {
        LOG(ERROR) << argv[0] << " ublox /dev/tty* BaudRate";
        return 1;
    }

    SerialFactory serialFactory;
    BaseSerial *pSerial = serialFactory.CreateSerial(argv[1]);

    pSerial->SetSerialDevice(argv[2]);
    pSerial->SetBaudRate(argv[3]);
    pSerial->SetOutputFileName();
    pSerial->Run();

    delete pSerial;
    pSerial = NULL;
    return 0;
}


