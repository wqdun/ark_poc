cmake_minimum_required(VERSION 2.8.3)
project(ublox)

add_compile_options(-std=c++11)

message(STATUS "PROJECT_SOURCE_DIR: " ${PROJECT_SOURCE_DIR})
add_executable(${PROJECT_NAME}_node
    ${PROJECT_SOURCE_DIR}/serial_node.cpp
    ${PROJECT_SOURCE_DIR}/serial_factory.cpp
    ${PROJECT_SOURCE_DIR}/serial_ublox.cpp
    ${PROJECT_SOURCE_DIR}/serial_ublox_hex.cpp
    ${PROJECT_SOURCE_DIR}/base_serial.cpp
    ${PROJECT_SOURCE_DIR}/tools_no_ros.cpp
)

target_link_libraries(${PROJECT_NAME}_node
    glog
)

add_executable(is_ublox_dat_valid
    ${PROJECT_SOURCE_DIR}/is_ublox_dat_valid.cpp
)

target_link_libraries(is_ublox_dat_valid
    glog
)



