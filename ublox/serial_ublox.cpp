#include "serial_ublox.h"

#include <sys/time.h>
#include <assert.h>

#include <boost/algorithm/string.hpp>


SerialUblox::~SerialUblox() {
    LOG(INFO) << "Goodbye SerialUblox..";
}

void SerialUblox::SetOutputFileName() {
    LOG(INFO) << __FUNCTION__ << " start.";

    struct timeval now;
    gettimeofday(&now, NULL);
    const double unixTime = now.tv_sec + now.tv_usec / 1000000.;

    char unixTimeCstr[40];
    sprintf(unixTimeCstr, "%017.6f", unixTime);

    outputFileName_ = std::string(unixTimeCstr) + "_ublox";
}

void SerialUblox::Run() {
    LOG(INFO) << __FUNCTION__ << " start.";
    buffer2File_.clear();

    const int fd = open(serialDevice_.c_str(), O_RDWR | O_NOCTTY);
    if(fd < 0) {
        LOG(ERROR) << "Failed to open " << serialDevice_ << ", try change permission...";
        LOG(ERROR) << "You may need to: sudo chown -R $USER " << serialDevice_;
        return;
    }

    if(public_tools::ToolsNoRos::SetSerialOption(fd, baudRate_, 8, 'N', 1) < 0) {
        LOG(ERROR) << "Failed to setup " << ttyname(fd);
        close(fd);
        return;
    }

    (void)ReadSerial(fd);

    close(fd);
    return;
}

void SerialUblox::ReadSerial(int _fd) {
    LOG(INFO) << __FUNCTION__ << " start.";

    const size_t BUFFER_SIZE = 10000;
    unsigned char buf[BUFFER_SIZE];
    bool isFirstFrame = true;
    std::string frameBuf("");
    double unixTime = -1.;

    int err = tcflush(_fd, TCIOFLUSH);
    LOG(INFO) << "tcflush: " << err;
    while (true) {
        usleep(500000);
        bzero(buf, BUFFER_SIZE);
        int nread = read(_fd, buf, BUFFER_SIZE);
        LOG(INFO) << "nread: " << nread;
        if(nread <= 0) {
            continue;
        }

        for(size_t i = 0; i < nread; ++i) {
            bool isFrameCompleted = false;
            switch(buf[i]) {
            case '$': {
                struct timeval now;
                gettimeofday(&now, NULL);
                unixTime = now.tv_sec + now.tv_usec / 1000000.;
                isFirstFrame = false;
                frameBuf = buf[i];
                break;
            }
            case '\r':
                break;
            case '\n':
                isFrameCompleted = true;
                break;
            default:
                frameBuf += buf[i];
            }

            if(isFirstFrame) {
                LOG_EVERY_N(INFO, 10) << "First frame might be incomplete, abandon: " << frameBuf;
                frameBuf.clear();
                continue;
            }

            if(!isFrameCompleted) {
                continue;
            }

            ParseFrame(frameBuf, unixTime);
            frameBuf.clear();
        }
    }
    return;
}

void SerialUblox::ParseFrame(const std::string &_frame, double _unixTime) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    assert(!_frame.empty() );
    WriteImuFile(_frame, _unixTime);

    if ("$GNGGA" == _frame.substr(0, 6)) {
        if (ParseGnggaFrame(_frame, _unixTime)) {
        }
        else {
            LOG(ERROR) << "Failed to parse " << _frame;
        }
    }
    else {
        LOG(WARNING) << "Unhandled frame: " << _frame;
    }

    return;
}

void SerialUblox::WriteImuFile(const std::string &_frame, double _unixTime) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    const std::string frameWithUnixtime(_frame + "," + std::to_string(_unixTime));
    buffer2File_.push_back(frameWithUnixtime);

    if (buffer2File_.size() >= 20) {
        const std::string imuFileName(outputFileName_ + ".txt");
        std::fstream file(imuFileName, std::ios::out | std::ios::app);
        if (!file) {
            LOG(ERROR) << "Failed to open " << imuFileName;
            exit(1);
        }

        std::string manyFrames("");
        for (const auto &oneFrame: buffer2File_) {
            manyFrames += (oneFrame + "\n");
        }
        file << manyFrames;
        file.close();
        buffer2File_.clear();
    } // else no write

    return;
}

bool SerialUblox::ParseGnggaFrame(const std::string &_gnggaFrame, double __unixTime) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    std::vector<std::string> gnggaFrameParsed;
    boost::split(gnggaFrameParsed, _gnggaFrame, boost::is_any_of(",*") );
    if (16 != gnggaFrameParsed.size()) {
        LOG(ERROR) << "Error parsing " << _gnggaFrame << "; gnggaFrameParsed.size(): " << gnggaFrameParsed.size();
        return false;
    }

    // e.g., $GNGGA,113113.00,4004.32673,N,11614.29327,E,1,06,2.11,-169.6,M,-9.1,M,,*7B
    const std::string ggaUtcTimeStr(gnggaFrameParsed[1]);
    const double ggaUtcTime = public_tools::ToolsNoRos::string2double(ggaUtcTimeStr);
    const double ggaUtcDaySec = UtcTime2DaySec(ggaUtcTime);

    unixTimeMinusGpsTimeQueue_.push_back(__unixTime - ggaUtcDaySec);
    if (unixTimeMinusGpsTimeQueue_.size() > 50) {
        unixTimeMinusGpsTimeQueue_.pop_front();
    }
    // else do nothing

    const double unixTimeMinusGpsTimeFiltered = FilterDeque(unixTimeMinusGpsTimeQueue_);
    WriteTimeErrFile(__unixTime, unixTimeMinusGpsTimeFiltered);
    return true;
}

// 120005.00 --> 12 * 3600 + 0 * 60 + 5.00
double SerialUblox::UtcTime2DaySec(double ggaUtcTime) {
    const int hour = (int)ggaUtcTime / 10000;
    const int minute = (int)ggaUtcTime / 100 % 100;
    const double second = fmod(ggaUtcTime, 100.);

    return (hour * 3600 + minute * 60 + second);
}

