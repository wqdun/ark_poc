#ifndef __BASE_SERIAL_H__
#define __BASE_SERIAL_H__

#include <glog/logging.h>
#include <string>
#include <deque>

class BaseSerial {
public:
    virtual ~BaseSerial();

    void SetSerialDevice(const std::string &serialDevice);
    void SetBaudRate(const std::string &baudRate);
    virtual void SetOutputFileName() = 0;

    virtual void Run() = 0;


protected:
    std::string serialDevice_;
    int baudRate_;
    std::string outputFileName_;

    std::deque<double> unixTimeMinusGpsTimeQueue_;

    double FilterDeque(const std::deque<double> &aDeque);
    void WriteTimeErrFile(double unixTime, double timeErr);
};

#endif
