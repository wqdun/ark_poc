#include <glog/logging.h>
#include <assert.h>

#include <iostream>
#include <stdio.h>
#include <cstdlib>


using namespace std;

typedef struct {
    int64_t isSolution;
    double timeStamp;
    unsigned char data[1206];
} pktInfo_t;

typedef union {
    unsigned char uCharData[2];
    unsigned short uShortData;
} uchar2Ushort_t;

typedef union {
    unsigned char uCharData[4];
    int32_t int32Data;
} uchar2int32_t;

typedef union {
    unsigned char uCharData[4];
    uint32_t uInt32Data;
} uchar2uInt32_t;

typedef union {
    unsigned char uCharData[4];
    float floatData;
} uchar2Float_t;

typedef union {
    unsigned char uCharData[8];
    double doubleData;
} uchar2Double_t;

static uint32_t GetItowInMs(unsigned char iTOW_0, unsigned char iTOW_1, unsigned char iTOW_2, unsigned char iTOW_3) {
    uchar2uInt32_t uchar2uInt32;
    uchar2uInt32.uCharData[0] = iTOW_0;
    uchar2uInt32.uCharData[1] = iTOW_1;
    uchar2uInt32.uCharData[2] = iTOW_2;
    uchar2uInt32.uCharData[3] = iTOW_3;

    const uint32_t itowInMs = uchar2uInt32.uInt32Data;
    return itowInMs;
}

static unsigned short CalcPayloadLength(unsigned char payloadLength_0, unsigned char payloadLength_1) {
    uchar2Ushort_t uchar2Ushort;
    uchar2Ushort.uCharData[0] = payloadLength_0;
    uchar2Ushort.uCharData[1] = payloadLength_1;
    unsigned short payloadLength = uchar2Ushort.uShortData;

    return payloadLength;
}

static void ParseFrame(const std::string &_frame) {
    LOG(INFO) << __FUNCTION__ << " start.";

    assert(_frame.size() > 17);
    const char MESSAGE_CLASS_HNR = 0x28;
    const char MESSAGE_CLASS_NAV = 0x01;

    const char MESSAGE_ID_ATT = 0x01;
    const char MESSAGE_ID_INS = 0x02;
    const char MESSAGE_ID_HNR = 0x37;

    double gpsTime2update = -1.;
    if ((MESSAGE_CLASS_HNR == _frame[2]) && (MESSAGE_ID_ATT == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[6], _frame[7], _frame[8], _frame[9]) / 1000.;
        LOG(INFO) << "Got a HNR-ATT, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else
    if ((MESSAGE_CLASS_HNR == _frame[2]) && (MESSAGE_ID_INS == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[14], _frame[15], _frame[16], _frame[17]) / 1000.;
        LOG(INFO) << "Got a HNR-INS, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else
    if  ((MESSAGE_CLASS_NAV == _frame[2]) && (MESSAGE_ID_HNR == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[6], _frame[7], _frame[8], _frame[9]) / 1000.;
        LOG(INFO) << "Got a NAV-HNR, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else {
        LOG(ERROR) << "Unhandled messageID: " << std::hex << (int)_frame[2] << (int)_frame[3];
    }
}


int main(int argc, char **argv) {
    if(2 != argc) {
        cout << "Param error.\n";
        return -1;
    }

    FILE *pInFile = fopen(argv[1], "rb");
    if(!pInFile) {
        cout << "Open " << argv[1] << " error.\n";
        return -1;
    }

    fseek(pInFile, 0, SEEK_END);
    long fileSize = ftell(pInFile);
    cout << "fileSize:" << fileSize << "\n";
    fseek(pInFile, 0, SEEK_SET);

    const char HEADER_0 = 0xb5;
    const char HEADER_1 = 0x62;
    const size_t HEADER_LENGTH = 6;
    const size_t CHECKSUM_LENGTH = 2;

    const size_t BUFFER_SIZE = 10000;
    unsigned char buf[BUFFER_SIZE];
    std::string framesBuf("");

    size_t pos = 0;
    size_t readLen = 0;
    do {
        bzero(buf, BUFFER_SIZE);
        readLen = fread(buf, 1, 200, pInFile);
        LOG(INFO) << "readLen: " << readLen;
        if (0 >= readLen) {
            LOG(ERROR) << pos << " end.";
            exit(1);
        }
        pos += readLen;

        for(int i = 0; i < readLen; ++i) {
            framesBuf += buf[i];
        }


        size_t i = 0;
        for ( ; i < framesBuf.size() - 5; ++i) {
            if ((HEADER_0 == framesBuf[i]) && (HEADER_1 == framesBuf[i + 1])) {
                LOG(INFO) << "Hello.";
                const unsigned short payloadLength = CalcPayloadLength(framesBuf[i + 4], framesBuf[i + 5]);
                const size_t frameLength = HEADER_LENGTH + payloadLength + CHECKSUM_LENGTH;
                LOG(INFO) << "i: " << i << "; frameLength: " << frameLength << "; HEADER_LENGTH: " << (int)HEADER_LENGTH << "; payloadLength: " << (int)payloadLength;

                if ((i + frameLength) > framesBuf.size()) {
                    LOG(INFO) << "The last frame is not complete: " << i << ":" << frameLength;
                    break;
                }

                std::string aFrame(framesBuf.substr(i, frameLength));
                ParseFrame(aFrame);

                i += frameLength;
                --i;
            }
        }

        framesBuf.erase(0, i);
    }
    while (pos < fileSize);

    fclose(pInFile);
    return 0;
}

