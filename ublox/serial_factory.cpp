#include "serial_factory.h"

#include "serial_ublox.h"
#include "serial_ublox_hex.h"


BaseSerial *SerialFactory::CreateSerial(const std::string &serialType) {
    if ("ublox" == serialType) {
        return new SerialUblox();
    }

    if ("ublox_hex" == serialType) {
        return new SerialUbloxHex();
    }

    LOG(ERROR) << "Unsupported type: " << serialType;
    exit(1);
}