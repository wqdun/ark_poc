#include "serial_ublox_hex.h"

#include <sys/time.h>
#include <assert.h>

#include <boost/algorithm/string.hpp>

typedef union {
    unsigned char uCharData[2];
    unsigned short uShortData;
} uchar2Ushort_t;

typedef union {
    unsigned char uCharData[4];
    int32_t int32Data;
} uchar2int32_t;

typedef union {
    unsigned char uCharData[4];
    uint32_t uInt32Data;
} uchar2uInt32_t;

typedef union {
    unsigned char uCharData[4];
    float floatData;
} uchar2Float_t;

typedef union {
    unsigned char uCharData[8];
    double doubleData;
} uchar2Double_t;

static uint32_t GetItowInMs(unsigned char iTOW_0, unsigned char iTOW_1, unsigned char iTOW_2, unsigned char iTOW_3) {
    uchar2uInt32_t uchar2uInt32;
    uchar2uInt32.uCharData[0] = iTOW_0;
    uchar2uInt32.uCharData[1] = iTOW_1;
    uchar2uInt32.uCharData[2] = iTOW_2;
    uchar2uInt32.uCharData[3] = iTOW_3;

    const uint32_t itowInMs = uchar2uInt32.uInt32Data;
    return itowInMs;
}

static unsigned short CalcPayloadLength(unsigned char payloadLength_0, unsigned char payloadLength_1) {
    uchar2Ushort_t uchar2Ushort;
    uchar2Ushort.uCharData[0] = payloadLength_0;
    uchar2Ushort.uCharData[1] = payloadLength_1;
    unsigned short payloadLength = uchar2Ushort.uShortData;

    return payloadLength;
}

static bool IsCheckSumRight(const std::string &frame) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    const size_t FRAME_SIZE = frame.size();
    const unsigned char CHECKSUM_A = frame[FRAME_SIZE - 2];
    const unsigned char CHECKSUM_B = frame[FRAME_SIZE - 1];

    unsigned char csA = 0;
    unsigned char csB = 0;
    for (size_t i = 2; i < FRAME_SIZE - 2; ++i) {
        csA += frame[i];
        csB += csA;
    }

    return (
        (CHECKSUM_A == csA) &&
        (CHECKSUM_B == csB)
    );
}


SerialUbloxHex::~SerialUbloxHex() {
    LOG(INFO) << "Goodbye SerialUbloxHex..";
}

void SerialUbloxHex::SetOutputFileName() {
    LOG(INFO) << __FUNCTION__ << " start.";

    struct timeval now;
    gettimeofday(&now, NULL);
    const double unixTime = now.tv_sec + now.tv_usec / 1000000.;

    char unixTimeCstr[40];
    sprintf(unixTimeCstr, "%017.6f", unixTime);

    outputFileName_ = std::string(unixTimeCstr) + "_ublox_hex";
}

void SerialUbloxHex::Run() {
    LOG(INFO) << __FUNCTION__ << " start.";
    buffer2File_.clear();

    const int fd = open(serialDevice_.c_str(), O_RDWR | O_NOCTTY);
    if (fd < 0) {
        LOG(ERROR) << "Failed to open " << serialDevice_ << ", try change permission...";
        LOG(ERROR) << "You may need to: sudo chown -R $USER " << serialDevice_;
        return;
    }

    if (public_tools::ToolsNoRos::SetSerialOption(fd, baudRate_, 8, 'N', 1) < 0) {
        LOG(ERROR) << "Failed to setup " << ttyname(fd);
        close(fd);
        return;
    }

    (void)ReadSerial(fd);

    close(fd);
    return;
}

void SerialUbloxHex::ReadSerial(int _fd) {
    LOG(INFO) << __FUNCTION__ << " start.";
    const char HEADER_0 = 0xb5;
    const char HEADER_1 = 0x62;
    const size_t HEADER_LENGTH = 6;
    const size_t CHECKSUM_LENGTH = 2;

    const size_t BUFFER_SIZE = 10000;
    unsigned char buf[BUFFER_SIZE];
    std::string framesBuf("");
    int err = tcflush(_fd, TCIOFLUSH);
    LOG(INFO) << "tcflush: " << err;
    err = tcflush(_fd, TCIFLUSH);
    LOG(INFO) << "tcflush: " << err;
    err = tcflush(_fd, TCOFLUSH);
    LOG(INFO) << "tcflush: " << err;
    while (true) {
        usleep(25000);
        bzero(buf, BUFFER_SIZE);
        int nread = read(_fd, buf, BUFFER_SIZE);
        LOG_EVERY_N(INFO, 50) << "nread: " << nread;
        if(nread <= 0) {
            continue;
        }

        for(int i = 0; i < nread; ++i) {
            framesBuf += buf[i];
        }

        if(framesBuf.size() < 100) {
            continue;
        }

        struct timeval now;
        gettimeofday(&now, NULL);
        const double unixTimeWhenIGetTheFrame = now.tv_sec + now.tv_usec / 1000000.;
#ifndef NDEBUG
        for (size_t i = 0; i < framesBuf.size(); ++i) {
            LOG(INFO) << i << ":" << std::hex << (int)framesBuf[i];
        }
#endif
        LOG_EVERY_N(INFO, 100) << "framesBuf.size(): " << framesBuf.size();
        double biggestGpsTimeOfWeekInS = -1;
        size_t bufIndex = 0;
        for ( ; bufIndex < framesBuf.size() - 5; ++bufIndex) {
            if ((HEADER_0 == framesBuf[bufIndex]) && (HEADER_1 == framesBuf[bufIndex + 1])) {
                const unsigned short payloadLength = CalcPayloadLength(framesBuf[bufIndex + 4], framesBuf[bufIndex + 5]);
                if (payloadLength > 200) {
                    LOG(ERROR) << "bufIndex: " << bufIndex << ": too long payloadLength: " << (int)payloadLength << "; skip header to parse: " << std::hex << (int)framesBuf[bufIndex + 2] << ", " << (int)framesBuf[bufIndex + 3];
                    ++bufIndex;
                    continue;
                }

                const size_t frameLength = HEADER_LENGTH + payloadLength + CHECKSUM_LENGTH;
                DLOG(INFO) << "bufIndex: " << bufIndex << "; frameLength: " << frameLength << "; HEADER_LENGTH: " << (int)HEADER_LENGTH << "; payloadLength: " << (int)payloadLength;
                if ( (bufIndex + frameLength) > framesBuf.size() ) {
                    LOG(INFO) << "The last frame is not complete: " << bufIndex << ":" << frameLength;
                    break;
                }

                const std::string aFrame(framesBuf.substr(bufIndex, frameLength));
                if (ParseFrame(aFrame, biggestGpsTimeOfWeekInS)) {
                    DLOG(INFO) << "Avoid unnecessary loop: " << bufIndex << ":" << frameLength;
                    bufIndex += frameLength;
                    --bufIndex;
                }
                else {
                    DLOG(INFO) << "Failed to ParseFrame.";
                }
            }
            else {
                LOG(WARNING) << "Failed to find sync code: " << std::hex << (int)framesBuf[bufIndex];
            }
        }
        LOG(INFO) << "Erase parsed characters, bufIndex: " << bufIndex;
        framesBuf.erase(0, bufIndex);

        if (biggestGpsTimeOfWeekInS > 0) {
            unixTimeMinusGpsTimeQueue_.push_back(unixTimeWhenIGetTheFrame - biggestGpsTimeOfWeekInS);
            if (unixTimeMinusGpsTimeQueue_.size() > 50) {
                unixTimeMinusGpsTimeQueue_.pop_front();
            }
            // else do nothing: keep size() == 50
            const double unixTimeMinusGpsTimeFiltered = FilterDeque(unixTimeMinusGpsTimeQueue_);
            WriteTimeErrFile(unixTimeWhenIGetTheFrame, unixTimeMinusGpsTimeFiltered);
        }
        // else GPS time is not updated: no need write time error file
    }
}

bool SerialUbloxHex::ParseFrame(const std::string &_frame, double &gpsTime2update) {
    DLOG(INFO) << __FUNCTION__ << " start.";
#ifndef NDEBUG
    for (auto &c: _frame) {
        LOG(INFO) << std::hex << (int)c;
    }
#endif
    if (!IsCheckSumRight(_frame)) {
        LOG(ERROR) << "Failed to IsCheckSumRight.";
        return false;
    }
    WriteImuHexFile(_frame);

    assert(_frame.size() > 17);
    const char MESSAGE_CLASS_HNR = 0x28;
    const char MESSAGE_CLASS_NAV = 0x01;

    const char MESSAGE_ID_ATT = 0x01;
    const char MESSAGE_ID_INS = 0x02;
    const char MESSAGE_ID_HNR = 0x37;

    if ((MESSAGE_CLASS_HNR == _frame[2]) && (MESSAGE_ID_ATT == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[6], _frame[7], _frame[8], _frame[9]) / 1000.;
        LOG(INFO) << "Got a HNR-ATT, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else
    if ((MESSAGE_CLASS_HNR == _frame[2]) && (MESSAGE_ID_INS == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[14], _frame[15], _frame[16], _frame[17]) / 1000.;
        LOG(INFO) << "Got a HNR-INS, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else
    if  ((MESSAGE_CLASS_NAV == _frame[2]) && (MESSAGE_ID_HNR == _frame[3])) {
        gpsTime2update = GetItowInMs(_frame[6], _frame[7], _frame[8], _frame[9]) / 1000.;
        LOG(INFO) << "Got a NAV-HNR, gpsTime2update: " << std::fixed << gpsTime2update;
    }
    else {
        LOG(ERROR) << "Unhandled messageID: " << std::hex << (int)_frame[2] << (int)_frame[3];
    }

    return true;
}

void SerialUbloxHex::WriteImuHexFile(const std::string &_frame) {
    DLOG(INFO) << __FUNCTION__ << " start.";

    buffer2File_.push_back(_frame);
    if (buffer2File_.size() < 20) {
        return;
    }

    std::string bigString("");
    for (auto &str: buffer2File_) {
        bigString += str;
    }

    const std::string imuFileName(outputFileName_ + ".dat");
    FILE *pOutFile;
    if (!(pOutFile = fopen(imuFileName.c_str(), "ab") ) ) {
        LOG(ERROR) << "Failed to create: " << imuFileName;
        exit(1);
    }

    const size_t byte2write = bigString.size();
    size_t writeByte = fwrite(bigString.c_str(), 1, byte2write, pOutFile);
    if (writeByte != byte2write) {
        LOG(ERROR) << "Write error: " << byte2write << ": " << writeByte;
    }

    fclose(pOutFile);

    buffer2File_.clear();

    return;
}


