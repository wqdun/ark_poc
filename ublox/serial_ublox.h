#ifndef __SERIAL_UBLOX_H__
#define __SERIAL_UBLOX_H__

#include <glog/logging.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include "base_serial.h"
#include "tools_no_ros.h"


class SerialUblox: public BaseSerial {
public:
    virtual ~SerialUblox();

    virtual void SetOutputFileName();
    virtual void Run();


private:
    void ReadSerial(int _fd);

    void ParseFrame(const std::string &_frame, double _unixTime);
    bool ParseGnggaFrame(const std::string &_gnggaFrame, double __unixTime);

    void WriteImuFile(const std::string &_frame, double _unixTime);

    double UtcTime2DaySec(double ggaUtcTime);

    std::vector<std::string> buffer2File_;
};

#endif
