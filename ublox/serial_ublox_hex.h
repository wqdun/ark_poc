#ifndef __SERIAL_UBLOX_HEX_H__
#define __SERIAL_UBLOX_HEX_H__

#include <glog/logging.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include "base_serial.h"
#include "tools_no_ros.h"


class SerialUbloxHex: public BaseSerial {
public:
    virtual ~SerialUbloxHex();

    virtual void SetOutputFileName();
    virtual void Run();


private:
    void ReadSerial(int _fd);

    bool ParseFrame(const std::string &_frame, double &gpsTime2update);

    void WriteImuHexFile(const std::string &_frame);

    std::string serialName_;
    std::vector<std::string> buffer2File_;
};

#endif
